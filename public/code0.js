gdjs._49Code = {};
gdjs._49Code.GDvalisefondObjects1= [];
gdjs._49Code.GDvalisefondObjects2= [];
gdjs._49Code.GDNewTextObjects1= [];
gdjs._49Code.GDNewTextObjects2= [];
gdjs._49Code.GDLeftArrowObjects1= [];
gdjs._49Code.GDLeftArrowObjects2= [];
gdjs._49Code.GDRightArrowObjects1= [];
gdjs._49Code.GDRightArrowObjects2= [];
gdjs._49Code.GDPauseObjects1= [];
gdjs._49Code.GDPauseObjects2= [];
gdjs._49Code.GDApplyObjects1= [];
gdjs._49Code.GDApplyObjects2= [];
gdjs._49Code.GDRetryObjects1= [];
gdjs._49Code.GDRetryObjects2= [];
gdjs._49Code.GDStartButtonObjects1= [];
gdjs._49Code.GDStartButtonObjects2= [];


gdjs._49Code.mapOfGDgdjs_46_9549Code_46GDStartButtonObjects1Objects = Hashtable.newFrom({"StartButton": gdjs._49Code.GDStartButtonObjects1});
gdjs._49Code.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("StartButton"), gdjs._49Code.GDStartButtonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._49Code.mapOfGDgdjs_46_9549Code_46GDStartButtonObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Bonjour", true);
}}

}


};

gdjs._49Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._49Code.GDvalisefondObjects1.length = 0;
gdjs._49Code.GDvalisefondObjects2.length = 0;
gdjs._49Code.GDNewTextObjects1.length = 0;
gdjs._49Code.GDNewTextObjects2.length = 0;
gdjs._49Code.GDLeftArrowObjects1.length = 0;
gdjs._49Code.GDLeftArrowObjects2.length = 0;
gdjs._49Code.GDRightArrowObjects1.length = 0;
gdjs._49Code.GDRightArrowObjects2.length = 0;
gdjs._49Code.GDPauseObjects1.length = 0;
gdjs._49Code.GDPauseObjects2.length = 0;
gdjs._49Code.GDApplyObjects1.length = 0;
gdjs._49Code.GDApplyObjects2.length = 0;
gdjs._49Code.GDRetryObjects1.length = 0;
gdjs._49Code.GDRetryObjects2.length = 0;
gdjs._49Code.GDStartButtonObjects1.length = 0;
gdjs._49Code.GDStartButtonObjects2.length = 0;

gdjs._49Code.eventsList0(runtimeScene);

return;

}

gdjs['_49Code'] = gdjs._49Code;
