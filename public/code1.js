gdjs.BonjourCode = {};
gdjs.BonjourCode.GDvalisefondObjects1= [];
gdjs.BonjourCode.GDvalisefondObjects2= [];
gdjs.BonjourCode.GDvalisefondObjects3= [];
gdjs.BonjourCode.GDNewTextObjects1= [];
gdjs.BonjourCode.GDNewTextObjects2= [];
gdjs.BonjourCode.GDNewTextObjects3= [];
gdjs.BonjourCode.GDLeftArrowObjects1= [];
gdjs.BonjourCode.GDLeftArrowObjects2= [];
gdjs.BonjourCode.GDLeftArrowObjects3= [];
gdjs.BonjourCode.GDRightArrowObjects1= [];
gdjs.BonjourCode.GDRightArrowObjects2= [];
gdjs.BonjourCode.GDRightArrowObjects3= [];
gdjs.BonjourCode.GDPauseObjects1= [];
gdjs.BonjourCode.GDPauseObjects2= [];
gdjs.BonjourCode.GDPauseObjects3= [];
gdjs.BonjourCode.GDApplyObjects1= [];
gdjs.BonjourCode.GDApplyObjects2= [];
gdjs.BonjourCode.GDApplyObjects3= [];
gdjs.BonjourCode.GDRetryObjects1= [];
gdjs.BonjourCode.GDRetryObjects2= [];
gdjs.BonjourCode.GDRetryObjects3= [];


gdjs.BonjourCode.asyncCallback9344500 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Bibliotheque", true);
}}
gdjs.BonjourCode.eventsList0 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.BonjourCode.asyncCallback9344500(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.BonjourCode.asyncCallback9343828 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "Bonjour .m4a", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(0);
}
{ //Subevents
gdjs.BonjourCode.eventsList0(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.BonjourCode.eventsList1 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.BonjourCode.asyncCallback9343828(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.BonjourCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.BonjourCode.eventsList1(runtimeScene);} //End of subevents
}

}


};

gdjs.BonjourCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.BonjourCode.GDvalisefondObjects1.length = 0;
gdjs.BonjourCode.GDvalisefondObjects2.length = 0;
gdjs.BonjourCode.GDvalisefondObjects3.length = 0;
gdjs.BonjourCode.GDNewTextObjects1.length = 0;
gdjs.BonjourCode.GDNewTextObjects2.length = 0;
gdjs.BonjourCode.GDNewTextObjects3.length = 0;
gdjs.BonjourCode.GDLeftArrowObjects1.length = 0;
gdjs.BonjourCode.GDLeftArrowObjects2.length = 0;
gdjs.BonjourCode.GDLeftArrowObjects3.length = 0;
gdjs.BonjourCode.GDRightArrowObjects1.length = 0;
gdjs.BonjourCode.GDRightArrowObjects2.length = 0;
gdjs.BonjourCode.GDRightArrowObjects3.length = 0;
gdjs.BonjourCode.GDPauseObjects1.length = 0;
gdjs.BonjourCode.GDPauseObjects2.length = 0;
gdjs.BonjourCode.GDPauseObjects3.length = 0;
gdjs.BonjourCode.GDApplyObjects1.length = 0;
gdjs.BonjourCode.GDApplyObjects2.length = 0;
gdjs.BonjourCode.GDApplyObjects3.length = 0;
gdjs.BonjourCode.GDRetryObjects1.length = 0;
gdjs.BonjourCode.GDRetryObjects2.length = 0;
gdjs.BonjourCode.GDRetryObjects3.length = 0;

gdjs.BonjourCode.eventsList2(runtimeScene);

return;

}

gdjs['BonjourCode'] = gdjs.BonjourCode;
