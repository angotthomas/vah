gdjs.BibliothequeCode = {};
gdjs.BibliothequeCode.GDLeftArrowObjects1_1final = [];

gdjs.BibliothequeCode.GDRightArrowObjects1_1final = [];

gdjs.BibliothequeCode.GDvalisefondObjects1= [];
gdjs.BibliothequeCode.GDvalisefondObjects2= [];
gdjs.BibliothequeCode.GDvalisefondObjects3= [];
gdjs.BibliothequeCode.GDNewTextObjects1= [];
gdjs.BibliothequeCode.GDNewTextObjects2= [];
gdjs.BibliothequeCode.GDNewTextObjects3= [];
gdjs.BibliothequeCode.GDLeftArrowObjects1= [];
gdjs.BibliothequeCode.GDLeftArrowObjects2= [];
gdjs.BibliothequeCode.GDLeftArrowObjects3= [];
gdjs.BibliothequeCode.GDRightArrowObjects1= [];
gdjs.BibliothequeCode.GDRightArrowObjects2= [];
gdjs.BibliothequeCode.GDRightArrowObjects3= [];
gdjs.BibliothequeCode.GDPauseObjects1= [];
gdjs.BibliothequeCode.GDPauseObjects2= [];
gdjs.BibliothequeCode.GDPauseObjects3= [];
gdjs.BibliothequeCode.GDApplyObjects1= [];
gdjs.BibliothequeCode.GDApplyObjects2= [];
gdjs.BibliothequeCode.GDApplyObjects3= [];
gdjs.BibliothequeCode.GDRetryObjects1= [];
gdjs.BibliothequeCode.GDRetryObjects2= [];
gdjs.BibliothequeCode.GDRetryObjects3= [];


gdjs.BibliothequeCode.asyncCallback9352748 = function (runtimeScene, asyncObjectsList) {
}
gdjs.BibliothequeCode.eventsList0 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.BibliothequeCode.asyncCallback9352748(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.BibliothequeCode.mapOfGDgdjs_46BibliothequeCode_46GDApplyObjects1Objects = Hashtable.newFrom({"Apply": gdjs.BibliothequeCode.GDApplyObjects1});
gdjs.BibliothequeCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "HerosC", true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "LectureM", true);
}}

}


};gdjs.BibliothequeCode.mapOfGDgdjs_46BibliothequeCode_46GDRetryObjects1Objects = Hashtable.newFrom({"Retry": gdjs.BibliothequeCode.GDRetryObjects1});
gdjs.BibliothequeCode.mapOfGDgdjs_46BibliothequeCode_46GDLeftArrowObjects1Objects = Hashtable.newFrom({"LeftArrow": gdjs.BibliothequeCode.GDLeftArrowObjects1});
gdjs.BibliothequeCode.mapOfGDgdjs_46BibliothequeCode_46GDRightArrowObjects1Objects = Hashtable.newFrom({"RightArrow": gdjs.BibliothequeCode.GDRightArrowObjects1});
gdjs.BibliothequeCode.mapOfGDgdjs_46BibliothequeCode_46GDRightArrowObjects2Objects = Hashtable.newFrom({"RightArrow": gdjs.BibliothequeCode.GDRightArrowObjects2});
gdjs.BibliothequeCode.mapOfGDgdjs_46BibliothequeCode_46GDLeftArrowObjects2Objects = Hashtable.newFrom({"LeftArrow": gdjs.BibliothequeCode.GDLeftArrowObjects2});
gdjs.BibliothequeCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Histoires des quatri.m4a", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Composition des sixi.m4a", 1, false, 100, 1);
}}

}


};gdjs.BibliothequeCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Histoires des quatri.m4a", 1, false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}
{ //Subevents
gdjs.BibliothequeCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Apply"), gdjs.BibliothequeCode.GDApplyObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.BibliothequeCode.mapOfGDgdjs_46BibliothequeCode_46GDApplyObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}
{ //Subevents
gdjs.BibliothequeCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Retry"), gdjs.BibliothequeCode.GDRetryObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.BibliothequeCode.mapOfGDgdjs_46BibliothequeCode_46GDRetryObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Bonjour", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("LeftArrow"), gdjs.BibliothequeCode.GDLeftArrowObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.BibliothequeCode.mapOfGDgdjs_46BibliothequeCode_46GDLeftArrowObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) - 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("RightArrow"), gdjs.BibliothequeCode.GDRightArrowObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.BibliothequeCode.mapOfGDgdjs_46BibliothequeCode_46GDRightArrowObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) + 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) < 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) > 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(2);
}}

}


{

gdjs.BibliothequeCode.GDLeftArrowObjects1.length = 0;

gdjs.BibliothequeCode.GDRightArrowObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.BibliothequeCode.GDLeftArrowObjects1_1final.length = 0;
gdjs.BibliothequeCode.GDRightArrowObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("RightArrow"), gdjs.BibliothequeCode.GDRightArrowObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.BibliothequeCode.mapOfGDgdjs_46BibliothequeCode_46GDRightArrowObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.BibliothequeCode.GDRightArrowObjects2.length; j < jLen ; ++j) {
        if ( gdjs.BibliothequeCode.GDRightArrowObjects1_1final.indexOf(gdjs.BibliothequeCode.GDRightArrowObjects2[j]) === -1 )
            gdjs.BibliothequeCode.GDRightArrowObjects1_1final.push(gdjs.BibliothequeCode.GDRightArrowObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("LeftArrow"), gdjs.BibliothequeCode.GDLeftArrowObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.BibliothequeCode.mapOfGDgdjs_46BibliothequeCode_46GDLeftArrowObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.BibliothequeCode.GDLeftArrowObjects2.length; j < jLen ; ++j) {
        if ( gdjs.BibliothequeCode.GDLeftArrowObjects1_1final.indexOf(gdjs.BibliothequeCode.GDLeftArrowObjects2[j]) === -1 )
            gdjs.BibliothequeCode.GDLeftArrowObjects1_1final.push(gdjs.BibliothequeCode.GDLeftArrowObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.BibliothequeCode.GDLeftArrowObjects1_1final, gdjs.BibliothequeCode.GDLeftArrowObjects1);
gdjs.copyArray(gdjs.BibliothequeCode.GDRightArrowObjects1_1final, gdjs.BibliothequeCode.GDRightArrowObjects1);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.BibliothequeCode.eventsList2(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.BibliothequeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.BibliothequeCode.GDvalisefondObjects1.length = 0;
gdjs.BibliothequeCode.GDvalisefondObjects2.length = 0;
gdjs.BibliothequeCode.GDvalisefondObjects3.length = 0;
gdjs.BibliothequeCode.GDNewTextObjects1.length = 0;
gdjs.BibliothequeCode.GDNewTextObjects2.length = 0;
gdjs.BibliothequeCode.GDNewTextObjects3.length = 0;
gdjs.BibliothequeCode.GDLeftArrowObjects1.length = 0;
gdjs.BibliothequeCode.GDLeftArrowObjects2.length = 0;
gdjs.BibliothequeCode.GDLeftArrowObjects3.length = 0;
gdjs.BibliothequeCode.GDRightArrowObjects1.length = 0;
gdjs.BibliothequeCode.GDRightArrowObjects2.length = 0;
gdjs.BibliothequeCode.GDRightArrowObjects3.length = 0;
gdjs.BibliothequeCode.GDPauseObjects1.length = 0;
gdjs.BibliothequeCode.GDPauseObjects2.length = 0;
gdjs.BibliothequeCode.GDPauseObjects3.length = 0;
gdjs.BibliothequeCode.GDApplyObjects1.length = 0;
gdjs.BibliothequeCode.GDApplyObjects2.length = 0;
gdjs.BibliothequeCode.GDApplyObjects3.length = 0;
gdjs.BibliothequeCode.GDRetryObjects1.length = 0;
gdjs.BibliothequeCode.GDRetryObjects2.length = 0;
gdjs.BibliothequeCode.GDRetryObjects3.length = 0;

gdjs.BibliothequeCode.eventsList3(runtimeScene);

return;

}

gdjs['BibliothequeCode'] = gdjs.BibliothequeCode;
