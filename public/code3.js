gdjs.HerosCCode = {};
gdjs.HerosCCode.GDLeftArrowObjects1_1final = [];

gdjs.HerosCCode.GDRightArrowObjects1_1final = [];

gdjs.HerosCCode.GDvalisefondObjects1= [];
gdjs.HerosCCode.GDvalisefondObjects2= [];
gdjs.HerosCCode.GDvalisefondObjects3= [];
gdjs.HerosCCode.GDNewTextObjects1= [];
gdjs.HerosCCode.GDNewTextObjects2= [];
gdjs.HerosCCode.GDNewTextObjects3= [];
gdjs.HerosCCode.GDLeftArrowObjects1= [];
gdjs.HerosCCode.GDLeftArrowObjects2= [];
gdjs.HerosCCode.GDLeftArrowObjects3= [];
gdjs.HerosCCode.GDRightArrowObjects1= [];
gdjs.HerosCCode.GDRightArrowObjects2= [];
gdjs.HerosCCode.GDRightArrowObjects3= [];
gdjs.HerosCCode.GDPauseObjects1= [];
gdjs.HerosCCode.GDPauseObjects2= [];
gdjs.HerosCCode.GDPauseObjects3= [];
gdjs.HerosCCode.GDApplyObjects1= [];
gdjs.HerosCCode.GDApplyObjects2= [];
gdjs.HerosCCode.GDApplyObjects3= [];
gdjs.HerosCCode.GDRetryObjects1= [];
gdjs.HerosCCode.GDRetryObjects2= [];
gdjs.HerosCCode.GDRetryObjects3= [];


gdjs.HerosCCode.asyncCallback9371196 = function (runtimeScene, asyncObjectsList) {
}
gdjs.HerosCCode.eventsList0 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.HerosCCode.asyncCallback9371196(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.HerosCCode.asyncCallback9370364 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Une fée .m4a", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}
{ //Subevents
gdjs.HerosCCode.eventsList0(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.HerosCCode.eventsList1 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.HerosCCode.asyncCallback9370364(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.HerosCCode.mapOfGDgdjs_46HerosCCode_46GDApplyObjects1Objects = Hashtable.newFrom({"Apply": gdjs.HerosCCode.GDApplyObjects1});
gdjs.HerosCCode.mapOfGDgdjs_46HerosCCode_46GDRetryObjects1Objects = Hashtable.newFrom({"Retry": gdjs.HerosCCode.GDRetryObjects1});
gdjs.HerosCCode.mapOfGDgdjs_46HerosCCode_46GDLeftArrowObjects1Objects = Hashtable.newFrom({"LeftArrow": gdjs.HerosCCode.GDLeftArrowObjects1});
gdjs.HerosCCode.mapOfGDgdjs_46HerosCCode_46GDRightArrowObjects1Objects = Hashtable.newFrom({"RightArrow": gdjs.HerosCCode.GDRightArrowObjects1});
gdjs.HerosCCode.mapOfGDgdjs_46HerosCCode_46GDRightArrowObjects2Objects = Hashtable.newFrom({"RightArrow": gdjs.HerosCCode.GDRightArrowObjects2});
gdjs.HerosCCode.mapOfGDgdjs_46HerosCCode_46GDLeftArrowObjects2Objects = Hashtable.newFrom({"LeftArrow": gdjs.HerosCCode.GDLeftArrowObjects2});
gdjs.HerosCCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Une fée .m4a", 2, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Un prince .m4a", 2, false, 100, 1);
}}

}


};gdjs.HerosCCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "Quel est le héros de.m4a", 1, false, 100, 1);
}
{ //Subevents
gdjs.HerosCCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Apply"), gdjs.HerosCCode.GDApplyObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.HerosCCode.mapOfGDgdjs_46HerosCCode_46GDApplyObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "LieuC", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Retry"), gdjs.HerosCCode.GDRetryObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.HerosCCode.mapOfGDgdjs_46HerosCCode_46GDRetryObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Bonjour", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("LeftArrow"), gdjs.HerosCCode.GDLeftArrowObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.HerosCCode.mapOfGDgdjs_46HerosCCode_46GDLeftArrowObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) - 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("RightArrow"), gdjs.HerosCCode.GDRightArrowObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.HerosCCode.mapOfGDgdjs_46HerosCCode_46GDRightArrowObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) + 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) < 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) > 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(2);
}}

}


{

gdjs.HerosCCode.GDLeftArrowObjects1.length = 0;

gdjs.HerosCCode.GDRightArrowObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.HerosCCode.GDLeftArrowObjects1_1final.length = 0;
gdjs.HerosCCode.GDRightArrowObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("RightArrow"), gdjs.HerosCCode.GDRightArrowObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.HerosCCode.mapOfGDgdjs_46HerosCCode_46GDRightArrowObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.HerosCCode.GDRightArrowObjects2.length; j < jLen ; ++j) {
        if ( gdjs.HerosCCode.GDRightArrowObjects1_1final.indexOf(gdjs.HerosCCode.GDRightArrowObjects2[j]) === -1 )
            gdjs.HerosCCode.GDRightArrowObjects1_1final.push(gdjs.HerosCCode.GDRightArrowObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("LeftArrow"), gdjs.HerosCCode.GDLeftArrowObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.HerosCCode.mapOfGDgdjs_46HerosCCode_46GDLeftArrowObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.HerosCCode.GDLeftArrowObjects2.length; j < jLen ; ++j) {
        if ( gdjs.HerosCCode.GDLeftArrowObjects1_1final.indexOf(gdjs.HerosCCode.GDLeftArrowObjects2[j]) === -1 )
            gdjs.HerosCCode.GDLeftArrowObjects1_1final.push(gdjs.HerosCCode.GDLeftArrowObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.HerosCCode.GDLeftArrowObjects1_1final, gdjs.HerosCCode.GDLeftArrowObjects1);
gdjs.copyArray(gdjs.HerosCCode.GDRightArrowObjects1_1final, gdjs.HerosCCode.GDRightArrowObjects1);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.HerosCCode.eventsList2(runtimeScene);} //End of subevents
}

}


};

gdjs.HerosCCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.HerosCCode.GDvalisefondObjects1.length = 0;
gdjs.HerosCCode.GDvalisefondObjects2.length = 0;
gdjs.HerosCCode.GDvalisefondObjects3.length = 0;
gdjs.HerosCCode.GDNewTextObjects1.length = 0;
gdjs.HerosCCode.GDNewTextObjects2.length = 0;
gdjs.HerosCCode.GDNewTextObjects3.length = 0;
gdjs.HerosCCode.GDLeftArrowObjects1.length = 0;
gdjs.HerosCCode.GDLeftArrowObjects2.length = 0;
gdjs.HerosCCode.GDLeftArrowObjects3.length = 0;
gdjs.HerosCCode.GDRightArrowObjects1.length = 0;
gdjs.HerosCCode.GDRightArrowObjects2.length = 0;
gdjs.HerosCCode.GDRightArrowObjects3.length = 0;
gdjs.HerosCCode.GDPauseObjects1.length = 0;
gdjs.HerosCCode.GDPauseObjects2.length = 0;
gdjs.HerosCCode.GDPauseObjects3.length = 0;
gdjs.HerosCCode.GDApplyObjects1.length = 0;
gdjs.HerosCCode.GDApplyObjects2.length = 0;
gdjs.HerosCCode.GDApplyObjects3.length = 0;
gdjs.HerosCCode.GDRetryObjects1.length = 0;
gdjs.HerosCCode.GDRetryObjects2.length = 0;
gdjs.HerosCCode.GDRetryObjects3.length = 0;

gdjs.HerosCCode.eventsList3(runtimeScene);

return;

}

gdjs['HerosCCode'] = gdjs.HerosCCode;
