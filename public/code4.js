gdjs.LieuCCode = {};
gdjs.LieuCCode.GDLeftArrowObjects1_1final = [];

gdjs.LieuCCode.GDRightArrowObjects1_1final = [];

gdjs.LieuCCode.GDvalisefondObjects1= [];
gdjs.LieuCCode.GDvalisefondObjects2= [];
gdjs.LieuCCode.GDvalisefondObjects3= [];
gdjs.LieuCCode.GDNewTextObjects1= [];
gdjs.LieuCCode.GDNewTextObjects2= [];
gdjs.LieuCCode.GDNewTextObjects3= [];
gdjs.LieuCCode.GDLeftArrowObjects1= [];
gdjs.LieuCCode.GDLeftArrowObjects2= [];
gdjs.LieuCCode.GDLeftArrowObjects3= [];
gdjs.LieuCCode.GDRightArrowObjects1= [];
gdjs.LieuCCode.GDRightArrowObjects2= [];
gdjs.LieuCCode.GDRightArrowObjects3= [];
gdjs.LieuCCode.GDPauseObjects1= [];
gdjs.LieuCCode.GDPauseObjects2= [];
gdjs.LieuCCode.GDPauseObjects3= [];
gdjs.LieuCCode.GDApplyObjects1= [];
gdjs.LieuCCode.GDApplyObjects2= [];
gdjs.LieuCCode.GDApplyObjects3= [];
gdjs.LieuCCode.GDRetryObjects1= [];
gdjs.LieuCCode.GDRetryObjects2= [];
gdjs.LieuCCode.GDRetryObjects3= [];


gdjs.LieuCCode.asyncCallback9388092 = function (runtimeScene, asyncObjectsList) {
}
gdjs.LieuCCode.eventsList0 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.LieuCCode.asyncCallback9388092(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.LieuCCode.asyncCallback9387260 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Un chateau .m4a", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}
{ //Subevents
gdjs.LieuCCode.eventsList0(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.LieuCCode.eventsList1 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.LieuCCode.asyncCallback9387260(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.LieuCCode.mapOfGDgdjs_46LieuCCode_46GDApplyObjects1Objects = Hashtable.newFrom({"Apply": gdjs.LieuCCode.GDApplyObjects1});
gdjs.LieuCCode.mapOfGDgdjs_46LieuCCode_46GDRetryObjects1Objects = Hashtable.newFrom({"Retry": gdjs.LieuCCode.GDRetryObjects1});
gdjs.LieuCCode.mapOfGDgdjs_46LieuCCode_46GDLeftArrowObjects1Objects = Hashtable.newFrom({"LeftArrow": gdjs.LieuCCode.GDLeftArrowObjects1});
gdjs.LieuCCode.mapOfGDgdjs_46LieuCCode_46GDRightArrowObjects1Objects = Hashtable.newFrom({"RightArrow": gdjs.LieuCCode.GDRightArrowObjects1});
gdjs.LieuCCode.mapOfGDgdjs_46LieuCCode_46GDRightArrowObjects2Objects = Hashtable.newFrom({"RightArrow": gdjs.LieuCCode.GDRightArrowObjects2});
gdjs.LieuCCode.mapOfGDgdjs_46LieuCCode_46GDLeftArrowObjects2Objects = Hashtable.newFrom({"LeftArrow": gdjs.LieuCCode.GDLeftArrowObjects2});
gdjs.LieuCCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Un chateau .m4a", 2, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Une grotte .m4a", 2, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 3;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Une cabane .m4a", 2, false, 100, 1);
}}

}


};gdjs.LieuCCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "Dans quel lieu ton h.m4a", 1, false, 100, 1);
}
{ //Subevents
gdjs.LieuCCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Apply"), gdjs.LieuCCode.GDApplyObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.LieuCCode.mapOfGDgdjs_46LieuCCode_46GDApplyObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "PersonnageC", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Retry"), gdjs.LieuCCode.GDRetryObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.LieuCCode.mapOfGDgdjs_46LieuCCode_46GDRetryObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Bonjour", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("LeftArrow"), gdjs.LieuCCode.GDLeftArrowObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.LieuCCode.mapOfGDgdjs_46LieuCCode_46GDLeftArrowObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) - 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("RightArrow"), gdjs.LieuCCode.GDRightArrowObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.LieuCCode.mapOfGDgdjs_46LieuCCode_46GDRightArrowObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) + 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) < 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) > 3;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(3);
}}

}


{

gdjs.LieuCCode.GDLeftArrowObjects1.length = 0;

gdjs.LieuCCode.GDRightArrowObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.LieuCCode.GDLeftArrowObjects1_1final.length = 0;
gdjs.LieuCCode.GDRightArrowObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("RightArrow"), gdjs.LieuCCode.GDRightArrowObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.LieuCCode.mapOfGDgdjs_46LieuCCode_46GDRightArrowObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.LieuCCode.GDRightArrowObjects2.length; j < jLen ; ++j) {
        if ( gdjs.LieuCCode.GDRightArrowObjects1_1final.indexOf(gdjs.LieuCCode.GDRightArrowObjects2[j]) === -1 )
            gdjs.LieuCCode.GDRightArrowObjects1_1final.push(gdjs.LieuCCode.GDRightArrowObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("LeftArrow"), gdjs.LieuCCode.GDLeftArrowObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.LieuCCode.mapOfGDgdjs_46LieuCCode_46GDLeftArrowObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.LieuCCode.GDLeftArrowObjects2.length; j < jLen ; ++j) {
        if ( gdjs.LieuCCode.GDLeftArrowObjects1_1final.indexOf(gdjs.LieuCCode.GDLeftArrowObjects2[j]) === -1 )
            gdjs.LieuCCode.GDLeftArrowObjects1_1final.push(gdjs.LieuCCode.GDLeftArrowObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.LieuCCode.GDLeftArrowObjects1_1final, gdjs.LieuCCode.GDLeftArrowObjects1);
gdjs.copyArray(gdjs.LieuCCode.GDRightArrowObjects1_1final, gdjs.LieuCCode.GDRightArrowObjects1);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.LieuCCode.eventsList2(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.LieuCCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.LieuCCode.GDvalisefondObjects1.length = 0;
gdjs.LieuCCode.GDvalisefondObjects2.length = 0;
gdjs.LieuCCode.GDvalisefondObjects3.length = 0;
gdjs.LieuCCode.GDNewTextObjects1.length = 0;
gdjs.LieuCCode.GDNewTextObjects2.length = 0;
gdjs.LieuCCode.GDNewTextObjects3.length = 0;
gdjs.LieuCCode.GDLeftArrowObjects1.length = 0;
gdjs.LieuCCode.GDLeftArrowObjects2.length = 0;
gdjs.LieuCCode.GDLeftArrowObjects3.length = 0;
gdjs.LieuCCode.GDRightArrowObjects1.length = 0;
gdjs.LieuCCode.GDRightArrowObjects2.length = 0;
gdjs.LieuCCode.GDRightArrowObjects3.length = 0;
gdjs.LieuCCode.GDPauseObjects1.length = 0;
gdjs.LieuCCode.GDPauseObjects2.length = 0;
gdjs.LieuCCode.GDPauseObjects3.length = 0;
gdjs.LieuCCode.GDApplyObjects1.length = 0;
gdjs.LieuCCode.GDApplyObjects2.length = 0;
gdjs.LieuCCode.GDApplyObjects3.length = 0;
gdjs.LieuCCode.GDRetryObjects1.length = 0;
gdjs.LieuCCode.GDRetryObjects2.length = 0;
gdjs.LieuCCode.GDRetryObjects3.length = 0;

gdjs.LieuCCode.eventsList3(runtimeScene);

return;

}

gdjs['LieuCCode'] = gdjs.LieuCCode;
