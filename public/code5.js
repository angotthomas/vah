gdjs.PersonnageCCode = {};
gdjs.PersonnageCCode.GDLeftArrowObjects1_1final = [];

gdjs.PersonnageCCode.GDRightArrowObjects1_1final = [];

gdjs.PersonnageCCode.GDvalisefondObjects1= [];
gdjs.PersonnageCCode.GDvalisefondObjects2= [];
gdjs.PersonnageCCode.GDvalisefondObjects3= [];
gdjs.PersonnageCCode.GDNewTextObjects1= [];
gdjs.PersonnageCCode.GDNewTextObjects2= [];
gdjs.PersonnageCCode.GDNewTextObjects3= [];
gdjs.PersonnageCCode.GDLeftArrowObjects1= [];
gdjs.PersonnageCCode.GDLeftArrowObjects2= [];
gdjs.PersonnageCCode.GDLeftArrowObjects3= [];
gdjs.PersonnageCCode.GDRightArrowObjects1= [];
gdjs.PersonnageCCode.GDRightArrowObjects2= [];
gdjs.PersonnageCCode.GDRightArrowObjects3= [];
gdjs.PersonnageCCode.GDPauseObjects1= [];
gdjs.PersonnageCCode.GDPauseObjects2= [];
gdjs.PersonnageCCode.GDPauseObjects3= [];
gdjs.PersonnageCCode.GDApplyObjects1= [];
gdjs.PersonnageCCode.GDApplyObjects2= [];
gdjs.PersonnageCCode.GDApplyObjects3= [];
gdjs.PersonnageCCode.GDRetryObjects1= [];
gdjs.PersonnageCCode.GDRetryObjects2= [];
gdjs.PersonnageCCode.GDRetryObjects3= [];


gdjs.PersonnageCCode.asyncCallback9406100 = function (runtimeScene, asyncObjectsList) {
}
gdjs.PersonnageCCode.eventsList0 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.PersonnageCCode.asyncCallback9406100(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.PersonnageCCode.asyncCallback9405316 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Un troll .m4a", 2, false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}
{ //Subevents
gdjs.PersonnageCCode.eventsList0(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.PersonnageCCode.eventsList1 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.PersonnageCCode.asyncCallback9405316(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.PersonnageCCode.mapOfGDgdjs_46PersonnageCCode_46GDApplyObjects1Objects = Hashtable.newFrom({"Apply": gdjs.PersonnageCCode.GDApplyObjects1});
gdjs.PersonnageCCode.mapOfGDgdjs_46PersonnageCCode_46GDRetryObjects1Objects = Hashtable.newFrom({"Retry": gdjs.PersonnageCCode.GDRetryObjects1});
gdjs.PersonnageCCode.mapOfGDgdjs_46PersonnageCCode_46GDLeftArrowObjects1Objects = Hashtable.newFrom({"LeftArrow": gdjs.PersonnageCCode.GDLeftArrowObjects1});
gdjs.PersonnageCCode.mapOfGDgdjs_46PersonnageCCode_46GDRightArrowObjects1Objects = Hashtable.newFrom({"RightArrow": gdjs.PersonnageCCode.GDRightArrowObjects1});
gdjs.PersonnageCCode.mapOfGDgdjs_46PersonnageCCode_46GDRightArrowObjects2Objects = Hashtable.newFrom({"RightArrow": gdjs.PersonnageCCode.GDRightArrowObjects2});
gdjs.PersonnageCCode.mapOfGDgdjs_46PersonnageCCode_46GDLeftArrowObjects2Objects = Hashtable.newFrom({"LeftArrow": gdjs.PersonnageCCode.GDLeftArrowObjects2});
gdjs.PersonnageCCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Un troll .m4a", 2, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Un Bonhomme de neige.m4a", 2, false, 100, 1);
}}

}


};gdjs.PersonnageCCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "Quel personnage ton .m4a", 1, false, 100, 1);
}
{ //Subevents
gdjs.PersonnageCCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Apply"), gdjs.PersonnageCCode.GDApplyObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.PersonnageCCode.mapOfGDgdjs_46PersonnageCCode_46GDApplyObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "ObjetC", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Retry"), gdjs.PersonnageCCode.GDRetryObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.PersonnageCCode.mapOfGDgdjs_46PersonnageCCode_46GDRetryObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Bonjour", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("LeftArrow"), gdjs.PersonnageCCode.GDLeftArrowObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.PersonnageCCode.mapOfGDgdjs_46PersonnageCCode_46GDLeftArrowObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) - 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("RightArrow"), gdjs.PersonnageCCode.GDRightArrowObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.PersonnageCCode.mapOfGDgdjs_46PersonnageCCode_46GDRightArrowObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) + 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) < 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) > 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(2);
}}

}


{

gdjs.PersonnageCCode.GDLeftArrowObjects1.length = 0;

gdjs.PersonnageCCode.GDRightArrowObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.PersonnageCCode.GDLeftArrowObjects1_1final.length = 0;
gdjs.PersonnageCCode.GDRightArrowObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("RightArrow"), gdjs.PersonnageCCode.GDRightArrowObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.PersonnageCCode.mapOfGDgdjs_46PersonnageCCode_46GDRightArrowObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.PersonnageCCode.GDRightArrowObjects2.length; j < jLen ; ++j) {
        if ( gdjs.PersonnageCCode.GDRightArrowObjects1_1final.indexOf(gdjs.PersonnageCCode.GDRightArrowObjects2[j]) === -1 )
            gdjs.PersonnageCCode.GDRightArrowObjects1_1final.push(gdjs.PersonnageCCode.GDRightArrowObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("LeftArrow"), gdjs.PersonnageCCode.GDLeftArrowObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.PersonnageCCode.mapOfGDgdjs_46PersonnageCCode_46GDLeftArrowObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.PersonnageCCode.GDLeftArrowObjects2.length; j < jLen ; ++j) {
        if ( gdjs.PersonnageCCode.GDLeftArrowObjects1_1final.indexOf(gdjs.PersonnageCCode.GDLeftArrowObjects2[j]) === -1 )
            gdjs.PersonnageCCode.GDLeftArrowObjects1_1final.push(gdjs.PersonnageCCode.GDLeftArrowObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.PersonnageCCode.GDLeftArrowObjects1_1final, gdjs.PersonnageCCode.GDLeftArrowObjects1);
gdjs.copyArray(gdjs.PersonnageCCode.GDRightArrowObjects1_1final, gdjs.PersonnageCCode.GDRightArrowObjects1);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.PersonnageCCode.eventsList2(runtimeScene);} //End of subevents
}

}


};

gdjs.PersonnageCCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.PersonnageCCode.GDvalisefondObjects1.length = 0;
gdjs.PersonnageCCode.GDvalisefondObjects2.length = 0;
gdjs.PersonnageCCode.GDvalisefondObjects3.length = 0;
gdjs.PersonnageCCode.GDNewTextObjects1.length = 0;
gdjs.PersonnageCCode.GDNewTextObjects2.length = 0;
gdjs.PersonnageCCode.GDNewTextObjects3.length = 0;
gdjs.PersonnageCCode.GDLeftArrowObjects1.length = 0;
gdjs.PersonnageCCode.GDLeftArrowObjects2.length = 0;
gdjs.PersonnageCCode.GDLeftArrowObjects3.length = 0;
gdjs.PersonnageCCode.GDRightArrowObjects1.length = 0;
gdjs.PersonnageCCode.GDRightArrowObjects2.length = 0;
gdjs.PersonnageCCode.GDRightArrowObjects3.length = 0;
gdjs.PersonnageCCode.GDPauseObjects1.length = 0;
gdjs.PersonnageCCode.GDPauseObjects2.length = 0;
gdjs.PersonnageCCode.GDPauseObjects3.length = 0;
gdjs.PersonnageCCode.GDApplyObjects1.length = 0;
gdjs.PersonnageCCode.GDApplyObjects2.length = 0;
gdjs.PersonnageCCode.GDApplyObjects3.length = 0;
gdjs.PersonnageCCode.GDRetryObjects1.length = 0;
gdjs.PersonnageCCode.GDRetryObjects2.length = 0;
gdjs.PersonnageCCode.GDRetryObjects3.length = 0;

gdjs.PersonnageCCode.eventsList3(runtimeScene);

return;

}

gdjs['PersonnageCCode'] = gdjs.PersonnageCCode;
