gdjs.ObjetCCode = {};
gdjs.ObjetCCode.GDLeftArrowObjects1_1final = [];

gdjs.ObjetCCode.GDRightArrowObjects1_1final = [];

gdjs.ObjetCCode.GDvalisefondObjects1= [];
gdjs.ObjetCCode.GDvalisefondObjects2= [];
gdjs.ObjetCCode.GDvalisefondObjects3= [];
gdjs.ObjetCCode.GDNewTextObjects1= [];
gdjs.ObjetCCode.GDNewTextObjects2= [];
gdjs.ObjetCCode.GDNewTextObjects3= [];
gdjs.ObjetCCode.GDLeftArrowObjects1= [];
gdjs.ObjetCCode.GDLeftArrowObjects2= [];
gdjs.ObjetCCode.GDLeftArrowObjects3= [];
gdjs.ObjetCCode.GDRightArrowObjects1= [];
gdjs.ObjetCCode.GDRightArrowObjects2= [];
gdjs.ObjetCCode.GDRightArrowObjects3= [];
gdjs.ObjetCCode.GDPauseObjects1= [];
gdjs.ObjetCCode.GDPauseObjects2= [];
gdjs.ObjetCCode.GDPauseObjects3= [];
gdjs.ObjetCCode.GDApplyObjects1= [];
gdjs.ObjetCCode.GDApplyObjects2= [];
gdjs.ObjetCCode.GDApplyObjects3= [];
gdjs.ObjetCCode.GDRetryObjects1= [];
gdjs.ObjetCCode.GDRetryObjects2= [];
gdjs.ObjetCCode.GDRetryObjects3= [];


gdjs.ObjetCCode.asyncCallback9423036 = function (runtimeScene, asyncObjectsList) {
}
gdjs.ObjetCCode.eventsList0 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(2), (runtimeScene) => (gdjs.ObjetCCode.asyncCallback9423036(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.ObjetCCode.asyncCallback9422276 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Une baguette magique.m4a", 2, false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}
{ //Subevents
gdjs.ObjetCCode.eventsList0(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.ObjetCCode.eventsList1 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.ObjetCCode.asyncCallback9422276(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.ObjetCCode.mapOfGDgdjs_46ObjetCCode_46GDApplyObjects1Objects = Hashtable.newFrom({"Apply": gdjs.ObjetCCode.GDApplyObjects1});
gdjs.ObjetCCode.mapOfGDgdjs_46ObjetCCode_46GDRetryObjects1Objects = Hashtable.newFrom({"Retry": gdjs.ObjetCCode.GDRetryObjects1});
gdjs.ObjetCCode.mapOfGDgdjs_46ObjetCCode_46GDLeftArrowObjects1Objects = Hashtable.newFrom({"LeftArrow": gdjs.ObjetCCode.GDLeftArrowObjects1});
gdjs.ObjetCCode.mapOfGDgdjs_46ObjetCCode_46GDRightArrowObjects1Objects = Hashtable.newFrom({"RightArrow": gdjs.ObjetCCode.GDRightArrowObjects1});
gdjs.ObjetCCode.mapOfGDgdjs_46ObjetCCode_46GDRightArrowObjects2Objects = Hashtable.newFrom({"RightArrow": gdjs.ObjetCCode.GDRightArrowObjects2});
gdjs.ObjetCCode.mapOfGDgdjs_46ObjetCCode_46GDLeftArrowObjects2Objects = Hashtable.newFrom({"LeftArrow": gdjs.ObjetCCode.GDLeftArrowObjects2});
gdjs.ObjetCCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Une baguette magique.m4a", 2, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Une épée .m4a", 2, false, 100, 1);
}}

}


};gdjs.ObjetCCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "Quel objet est utili.m4a", 1, false, 100, 1);
}
{ //Subevents
gdjs.ObjetCCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Apply"), gdjs.ObjetCCode.GDApplyObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.ObjetCCode.mapOfGDgdjs_46ObjetCCode_46GDApplyObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "LectureC", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Retry"), gdjs.ObjetCCode.GDRetryObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.ObjetCCode.mapOfGDgdjs_46ObjetCCode_46GDRetryObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Bonjour", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("LeftArrow"), gdjs.ObjetCCode.GDLeftArrowObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.ObjetCCode.mapOfGDgdjs_46ObjetCCode_46GDLeftArrowObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) - 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("RightArrow"), gdjs.ObjetCCode.GDRightArrowObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.ObjetCCode.mapOfGDgdjs_46ObjetCCode_46GDRightArrowObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) + 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) < 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) > 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(2);
}}

}


{

gdjs.ObjetCCode.GDLeftArrowObjects1.length = 0;

gdjs.ObjetCCode.GDRightArrowObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.ObjetCCode.GDLeftArrowObjects1_1final.length = 0;
gdjs.ObjetCCode.GDRightArrowObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("RightArrow"), gdjs.ObjetCCode.GDRightArrowObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.ObjetCCode.mapOfGDgdjs_46ObjetCCode_46GDRightArrowObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.ObjetCCode.GDRightArrowObjects2.length; j < jLen ; ++j) {
        if ( gdjs.ObjetCCode.GDRightArrowObjects1_1final.indexOf(gdjs.ObjetCCode.GDRightArrowObjects2[j]) === -1 )
            gdjs.ObjetCCode.GDRightArrowObjects1_1final.push(gdjs.ObjetCCode.GDRightArrowObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("LeftArrow"), gdjs.ObjetCCode.GDLeftArrowObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.ObjetCCode.mapOfGDgdjs_46ObjetCCode_46GDLeftArrowObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.ObjetCCode.GDLeftArrowObjects2.length; j < jLen ; ++j) {
        if ( gdjs.ObjetCCode.GDLeftArrowObjects1_1final.indexOf(gdjs.ObjetCCode.GDLeftArrowObjects2[j]) === -1 )
            gdjs.ObjetCCode.GDLeftArrowObjects1_1final.push(gdjs.ObjetCCode.GDLeftArrowObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.ObjetCCode.GDLeftArrowObjects1_1final, gdjs.ObjetCCode.GDLeftArrowObjects1);
gdjs.copyArray(gdjs.ObjetCCode.GDRightArrowObjects1_1final, gdjs.ObjetCCode.GDRightArrowObjects1);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.ObjetCCode.eventsList2(runtimeScene);} //End of subevents
}

}


};

gdjs.ObjetCCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.ObjetCCode.GDvalisefondObjects1.length = 0;
gdjs.ObjetCCode.GDvalisefondObjects2.length = 0;
gdjs.ObjetCCode.GDvalisefondObjects3.length = 0;
gdjs.ObjetCCode.GDNewTextObjects1.length = 0;
gdjs.ObjetCCode.GDNewTextObjects2.length = 0;
gdjs.ObjetCCode.GDNewTextObjects3.length = 0;
gdjs.ObjetCCode.GDLeftArrowObjects1.length = 0;
gdjs.ObjetCCode.GDLeftArrowObjects2.length = 0;
gdjs.ObjetCCode.GDLeftArrowObjects3.length = 0;
gdjs.ObjetCCode.GDRightArrowObjects1.length = 0;
gdjs.ObjetCCode.GDRightArrowObjects2.length = 0;
gdjs.ObjetCCode.GDRightArrowObjects3.length = 0;
gdjs.ObjetCCode.GDPauseObjects1.length = 0;
gdjs.ObjetCCode.GDPauseObjects2.length = 0;
gdjs.ObjetCCode.GDPauseObjects3.length = 0;
gdjs.ObjetCCode.GDApplyObjects1.length = 0;
gdjs.ObjetCCode.GDApplyObjects2.length = 0;
gdjs.ObjetCCode.GDApplyObjects3.length = 0;
gdjs.ObjetCCode.GDRetryObjects1.length = 0;
gdjs.ObjetCCode.GDRetryObjects2.length = 0;
gdjs.ObjetCCode.GDRetryObjects3.length = 0;

gdjs.ObjetCCode.eventsList3(runtimeScene);

return;

}

gdjs['ObjetCCode'] = gdjs.ObjetCCode;
