gdjs.LectureMCode = {};
gdjs.LectureMCode.GDvalisefondObjects1= [];
gdjs.LectureMCode.GDvalisefondObjects2= [];
gdjs.LectureMCode.GDvalisefondObjects3= [];
gdjs.LectureMCode.GDNewTextObjects1= [];
gdjs.LectureMCode.GDNewTextObjects2= [];
gdjs.LectureMCode.GDNewTextObjects3= [];
gdjs.LectureMCode.GDLeftArrowObjects1= [];
gdjs.LectureMCode.GDLeftArrowObjects2= [];
gdjs.LectureMCode.GDLeftArrowObjects3= [];
gdjs.LectureMCode.GDRightArrowObjects1= [];
gdjs.LectureMCode.GDRightArrowObjects2= [];
gdjs.LectureMCode.GDRightArrowObjects3= [];
gdjs.LectureMCode.GDPauseObjects1= [];
gdjs.LectureMCode.GDPauseObjects2= [];
gdjs.LectureMCode.GDPauseObjects3= [];
gdjs.LectureMCode.GDApplyObjects1= [];
gdjs.LectureMCode.GDApplyObjects2= [];
gdjs.LectureMCode.GDApplyObjects3= [];
gdjs.LectureMCode.GDRetryObjects1= [];
gdjs.LectureMCode.GDRetryObjects2= [];
gdjs.LectureMCode.GDRetryObjects3= [];


gdjs.LectureMCode.asyncCallback9439652 = function (runtimeScene, asyncObjectsList) {
}
gdjs.LectureMCode.eventsList0 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(3), (runtimeScene) => (gdjs.LectureMCode.asyncCallback9439652(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.LectureMCode.asyncCallback9439276 = function (runtimeScene, asyncObjectsList) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "Compo_juin_6e3 (1)2.wav", 1, false, 100, 1);
}
{ //Subevents
gdjs.LectureMCode.eventsList0(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.LectureMCode.eventsList1 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.LectureMCode.asyncCallback9439276(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.LectureMCode.mapOfGDgdjs_46LectureMCode_46GDRetryObjects1Objects = Hashtable.newFrom({"Retry": gdjs.LectureMCode.GDRetryObjects1});
gdjs.LectureMCode.mapOfGDgdjs_46LectureMCode_46GDPauseObjects1Objects = Hashtable.newFrom({"Pause": gdjs.LectureMCode.GDPauseObjects1});
gdjs.LectureMCode.mapOfGDgdjs_46LectureMCode_46GDPauseObjects1Objects = Hashtable.newFrom({"Pause": gdjs.LectureMCode.GDPauseObjects1});
gdjs.LectureMCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.LectureMCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Retry"), gdjs.LectureMCode.GDRetryObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.LectureMCode.mapOfGDgdjs_46LectureMCode_46GDRetryObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "CMPTKey_Trackpad macbook simple clic (ID 1740)_LS.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Bonjour", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Pause"), gdjs.LectureMCode.GDPauseObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.LectureMCode.mapOfGDgdjs_46LectureMCode_46GDPauseObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelPlaying(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.LectureMCode.GDPauseObjects1 */
{gdjs.evtTools.sound.pauseMusicOnChannel(runtimeScene, 1);
}{for(var i = 0, len = gdjs.LectureMCode.GDPauseObjects1.length ;i < len;++i) {
    gdjs.LectureMCode.GDPauseObjects1[i].setColor("208;2;27");
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Pause"), gdjs.LectureMCode.GDPauseObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.LectureMCode.mapOfGDgdjs_46LectureMCode_46GDPauseObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelPaused(runtimeScene, 1);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.LectureMCode.GDPauseObjects1 */
{gdjs.evtTools.sound.continueMusicOnChannel(runtimeScene, 1);
}{for(var i = 0, len = gdjs.LectureMCode.GDPauseObjects1.length ;i < len;++i) {
    gdjs.LectureMCode.GDPauseObjects1[i].setColor("255;255;255");
}
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.LectureMCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.LectureMCode.GDvalisefondObjects1.length = 0;
gdjs.LectureMCode.GDvalisefondObjects2.length = 0;
gdjs.LectureMCode.GDvalisefondObjects3.length = 0;
gdjs.LectureMCode.GDNewTextObjects1.length = 0;
gdjs.LectureMCode.GDNewTextObjects2.length = 0;
gdjs.LectureMCode.GDNewTextObjects3.length = 0;
gdjs.LectureMCode.GDLeftArrowObjects1.length = 0;
gdjs.LectureMCode.GDLeftArrowObjects2.length = 0;
gdjs.LectureMCode.GDLeftArrowObjects3.length = 0;
gdjs.LectureMCode.GDRightArrowObjects1.length = 0;
gdjs.LectureMCode.GDRightArrowObjects2.length = 0;
gdjs.LectureMCode.GDRightArrowObjects3.length = 0;
gdjs.LectureMCode.GDPauseObjects1.length = 0;
gdjs.LectureMCode.GDPauseObjects2.length = 0;
gdjs.LectureMCode.GDPauseObjects3.length = 0;
gdjs.LectureMCode.GDApplyObjects1.length = 0;
gdjs.LectureMCode.GDApplyObjects2.length = 0;
gdjs.LectureMCode.GDApplyObjects3.length = 0;
gdjs.LectureMCode.GDRetryObjects1.length = 0;
gdjs.LectureMCode.GDRetryObjects2.length = 0;
gdjs.LectureMCode.GDRetryObjects3.length = 0;

gdjs.LectureMCode.eventsList2(runtimeScene);

return;

}

gdjs['LectureMCode'] = gdjs.LectureMCode;
